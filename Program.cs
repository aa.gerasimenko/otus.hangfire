using Hangfire;
using Hangfire.Storage.SQLite;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// add hangfire and sqlite storage
//GlobalConfiguration.Configuration
builder.Services.AddHangfire(c => c
    .SetDataCompatibilityLevel(CompatibilityLevel.Version_180)
    .UseColouredConsoleLogProvider()
    .UseSimpleAssemblyNameTypeSerializer()
    .UseRecommendedSerializerSettings()
    .UseSQLiteStorage());
builder.Services.AddHangfireServer();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.MapGet("/weatherforecast", () =>
{
    var summaries = new[]
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };
    var forecast = Enumerable.Range(1, 5).Select(index =>
        new WeatherForecast
        (
            DateOnly.FromDateTime(DateTime.Now.AddDays(index)),
            Random.Shared.Next(-20, 55),
            summaries[Random.Shared.Next(summaries.Length)]
        ))
        .ToArray();
    return forecast;
})
.WithName("GetWeatherForecast")
.WithOpenApi();

// add background job 
app.MapGet("/simple", () =>
    BackgroundJob.Enqueue(() => Console.WriteLine("Hello, world!"))
);

// scheduled job
app.MapGet("/schedule/{seconds}", (int seconds) =>
{
    var id = BackgroundJob.Schedule(
        () => Console.WriteLine("Hello, world! {0}", seconds),
        TimeSpan.FromSeconds(seconds)
    );

    return Results.Ok(id);
});

// recurring
app.MapGet("/recurring/{name}", (string name) =>
{
    RecurringJob.AddOrUpdate(name, () => Console.Write("Easy!"),"0/15 * * * * *");
    // RecurringJob.AddOrUpdate(name, () => Console.Write("Easy!"), Cron.Minutely);
});

app.MapGet("/recurring/{name}/cancel", (string name) =>
{
    RecurringJob.RemoveIfExists(name);
});

app.MapGet("/recurring/{name}/force", (string name) =>
{
    RecurringJob.TriggerJob(name);
});

// see dashboard http://localhost:5008/hangfire
app.UseHangfireDashboard();

app.Run();

record WeatherForecast(DateOnly Date, int TemperatureC, string? Summary)
{
    public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);
}
